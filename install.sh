#!/bin/sh

# Run this script on archiso (as root)

if [ -f `pwd`/scripts/utils ]; then
    source `pwd`/scripts/utils
else
    echo "Could not find utils functions"
    exit 1
fi

auto_install() {
    printf "Disk choice: /dev/$disk\n"
    run 'read -n 1 sure' "Is the disk choice correct (y/n)?"
    if [ "$format_answer" != "y" ]; then
        exit 1
    fi

    sfdisk /dev/$disk <<EOF
label:gpt
,+512M,U
;
EOF

    run "mkfs.vfat -F32 /dev/${disk}1" "Formatting boot partition to FAT32"
    run "mkfs.ext4 /dev/${disk}2" "Formatting root partition to ext4"
    run "mount /dev/${disk}2 /mnt" "Mounting root partition"
    run "mkdir -p /mnt/boot && mount /dev/${disk}1 /mnt/boot" "Mounting boot partition"
    run "sleep 1" "Installing base system"
    pacstrap /mnt base base-devel
    genfstab -U /mnt >> /mnt/etc/fstab
    run_chroot "ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime" "Setting timezone"
    run_chroot "hwclock --systohc" "Updating hardware clock"
    run_chroot 'sed -i -e "s/#en_US.UTF-8/en_US.UTF-8/g" /etc/locale.gen && locale-gen' "Updating locales"
    run_chroot 'echo "LANG=en_US.UTF-8" > /etc/locale.conf' "Setting LANG"
    run_chroot 'echo "KEYMAP=us"' 'Setting KEYMAP'
    run_chroot 'echo "$name" > /etc/hostname' 'Setting hostname'
    echo '127.0.0.1 localhost' >> /etc/hosts
    echo '::1 localhost' >> /etc/hosts
    run_chroot 'passwd' "Set your root password:"
    run 'read -n 1 is_vm' "Is this a virtual machine (y/n) ?"
    if [ "$is_vm" != "y" ]; then
        run_chroot 'pacman -S intel-ucode' "Installing Intel-ucode"
    fi
    run_chroot 'pacman -S vim' "Installing vim"
    run_chroot 'bootctl --path=/boot install' "Installing systemd-boot"
    run 'cp -r . /mnt/root/' "Copying installation files to new systems root home"
    }

run "loadkeys us" "Setting keymap to us"
run "test -e /sys/firmware/efi/efivars" "Checking EFI mode"
run "ping -q -c 1 -W 1 google.com" "Checking internet connection"
run "timedatectl set-ntp true" "Updating system clock"

sfdisk -l
run 'read disk' "Please choose your installation location: /dev/"
run 'read name' "Please choose your hostname: "
run 'read -n 1 format_answer' "Would you like to format the disk,create partitions, and install automatically ?"
if [ "$format_answer" = "y" ]; then
    auto_install
fi
